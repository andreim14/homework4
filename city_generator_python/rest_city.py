import json
import re
import sys
import time

import numpy as np
import osmnx as ox

from flask import request, Flask

app = Flask(__name__)

# Type of areas that are going to be green
grass_types = {"grass", "forest", "farmland", "village_green", "meadow",
               "orchard", "vineyard", "recreation_ground", "grassland"}

# Type of buildings that are going to be higher if they don't have a level set
high_buildings = {"apartments", "residential", "tower", "hotel"}

# Roads that for pedestrian so are not going to be represented as asphalt color
path_roads = {"service", "pedestrian", "track", "footway",
              "bridleway", "steps", "corridor", "path"}

# The distance to retrive the inforrmation from OSM represented in meters
distance = 2000

# The scale of the representation
scale = 50


class CoordStatistics:
    """
    Class that is keeping the relative min and max coordinates in order to scale
    the coordinates to fit them in the scene
    """
    def __init__(self):
        self.x_min = sys.float_info.max
        self.y_min = sys.float_info.max
        self.x_max = -sys.float_info.max
        self.y_max = -sys.float_info.max

    def set_x_min(self, x_min):
        if self.x_min > x_min:
            self.x_min = x_min

    def set_y_min(self, y_min):
        if self.y_min > y_min:
            self.y_min = y_min

    def set_y_max(self, y_max):
        if self.y_max < y_max:
            self.y_max = y_max

    def set_x_max(self, x_max):
        if self.x_max < x_max:
            self.x_max = x_max

    def update(self, x, y):
        self.set_x_max(x)
        self.set_x_min(x)
        self.set_y_max(y)
        self.set_y_min(y)


def generate_coordinates(point, all_buildings, coord_statistics,
                         distance=2000, footprint_type='building'):
    start = time.time()

    gdf = ox.footprints.footprints_from_point(point, distance,
                                              footprint_type=footprint_type)
    print(f"\tTime to retrieve {footprint_type}: {time.time() - start}")
    for index, row in gdf.iterrows():

        if row["geometry"].type == 'Polygon':
            polygons = [row["geometry"]]
        else:
            polygons = list(row["geometry"])
        multi_polygon_count = 0
        level = generate_building_level(footprint_type, row)

        for value in polygons:
            building = dict()
            if footprint_type == 'building':
                building["level"] = level
            exterior_coord = value.exterior.coords[:-1]

            if "landuse" not in row or footprint_type == "building" \
                    or footprint_type == "water" or footprint_type == "natural":
                building["type"] = footprint_type
            else:
                building["type"] = row["landuse"]

            if footprint_type == "natural":
                building["type"] = row["natural"]
            for x, y in exterior_coord:
                coord_statistics.update(x, y)
            building["coords"] = list(exterior_coord)

            building["name"] = "building_" + str(index) + str(multi_polygon_count)
            multi_polygon_count += 1
            if building["type"] == "building" or building["type"] in grass_types \
                    or building["type"] == "water" or building["type"] == "sand":
                all_buildings[building["name"]] = building


def generate_building_level(footprint_type, row):
    """
    Generate building levels


    :param footprint_type: the footprint type
    :param row:  the from the geojson that represents the building
    :return: the level
    """
    level = 1

    if "building:levels" in row:
        level = 1 if type(row["building:levels"]) == float or not row[
            "building:levels"].isnumeric() else int(row["building:levels"]) + 1

    height = None

    # Check if building instead of level has
    # height or building:height in meters
    #
    # A floor has almost 3.2 meters so to convert it
    # to floor level i have divded the height by 3.2 to obtain level
    if footprint_type == 'building' \
            and "height" in row \
            and level == 1:
        height = row["height"]
    if footprint_type == 'building' \
            and "building:height" in row \
            and level == 1:
        height = row["building:height"]
    if height:
        height = re.sub('[^0-9.]', '', str(height))
        if len(height) > 0 and height.isnumeric():
            level = int(float(height) / 3.2)

    # If is a building and has a known type that
    # has floors in reallife (but in OSM there are no data)
    # generate a random level
    if footprint_type == 'building' \
            and "building" in row \
            and level == 1 \
            and (row["building"] in high_buildings):
        level = np.random.uniform(5, 6, 1)[0]
    return level


def get_color(object_type, sub_type):
    """
    Returns the color of the object type

    :param object_type: the type of the object
    :param sub_type: the sub_type of the object
    :return: a color
    """
    if object_type == "building":
        return 0.79, 0.74, 0.62
    if object_type == "water":
        return 0.72, 0.95, 1
    if object_type == "sand":
        return 0.69, 0.58, 0.43
    if object_type == "highway":
        if sub_type in path_roads:
            return 0.45, 0.4, 0.27
        return 0.26, 0.26, 0.28
    if object_type in grass_types:
        return 0.337, 0.49, 0.274
    return 0.725, 0.71, 0.68


def get_subtype_thickness(sub_type):
    """
    Generates the subtype thickness of a road
    This is useful because some roads can have different thickness
    :param sub_type:
    :return:
    """
    line_size = 0.0001
    if str(sub_type) in path_roads:
        line_size = 0.00005

    return line_size


def generates_roads(G, all_buildings, coord_statistics):
    """
    Generates road geometry from the OSM graph

    Roads can have geometry or can be lines from node to node
    so i have generated gemetry for the roads

    :param G: the graph
    :param all_buildings: the geometry object lists
    :param coord_statistics: the object that retains the min and max coords
     (used to generate relative coordianates)
    :return: None
    """
    count = 0
    for u, v, data in G.edges(keys=False, data=True):
        sub_type = data["highway"]
        sub_type = sub_type if type(sub_type) == str else sub_type[0]
        line_size = get_subtype_thickness(sub_type)
        if "geometry" in data:
            # if road already have geometry
            xs, ys = data["geometry"].xy

            coords = list(zip(xs, ys))
            for i in range(len(coords) - 1):
                coord = coords[i]
                target = coords[i + 1]
                generate_line(line_size, coord[0], target[0], coord[1], target[1],
                              count, all_buildings, sub_type, coord_statistics)

                count += 1
        else:
            # if it doesn't have a geometry attribute, the edge is a straight
            # line from node to node
            generate_line(line_size, G.nodes[u]["x"], G.nodes[v]["x"], G.nodes[u]["y"], G.nodes[v]["y"],
                          count, all_buildings, sub_type, coord_statistics)
            count += 1


def polyArea(x, y):
    """
    Calculates the polygon area
    :param x: the list of x values
    :param y: the list of y values
    :return: the area of the polygon
    """
    return 0.5 * np.abs(np.dot(x, np.roll(y, 1)) - np.dot(y, np.roll(x, 1)))


def find_max_area(x1, x2, y1, y2, line_thickness):
    """
    In order to generate a road chose between the possible
    road representation in 2d space (chose the one that produces larger area of the polygon)

    this is useful because if i use only diagonal representation in some cases the roads wern't thick

    :return: the polygon of the line with larger area
    """
    line_diagonal = [(x2 + line_thickness, y2 + line_thickness), (x2 - line_thickness, y2 - line_thickness),
                     (x1 - line_thickness, y1 - line_thickness), (x1 + line_thickness, y1 + line_thickness)]
    list1, list2 = zip(*line_diagonal)
    area1 = polyArea(np.array(list1), np.array(list2))

    line_diagonal2 = [(x2 + line_thickness, y2), (x2 - line_thickness, y2),
                      (x1 - line_thickness, y1), (x1 + line_thickness, y1)]
    list1, list2 = zip(*line_diagonal2)

    area2 = polyArea(np.array(list1), np.array(list2))

    if area2 > area1:
        return line_diagonal2
    return line_diagonal


def generate_line(line_thickness, x1, x2, y1, y2, line_count,
                  all_geometry_obj, sub_type, coord_statistics):
    """
    Generate thick line prom points

    :param line_thickness: thickness of the line
    :param x1: x origin of the line
    :param x2: x target of the line
    :param y1: y origin of the line
    :param y2: y target of the line
    :param line_count: index of the line
    :param all_geometry_obj: collection of all objects
    :param sub_type: the subtype of the road
    :param coord_statistics:
    :return: None
    """

    line = find_max_area(x1, x2, y1, y2, line_thickness)

    id = "line_" + str(line_count)
    building = dict()
    building["name"] = id
    building["type"] = "highway"
    building["sub_type"] = sub_type if type(sub_type) == str else sub_type[0]
    building["coords"] = line
    all_geometry_obj[building["name"]] = building
    for x, y in line:
        coord_statistics.update(x, y)


def generate_material(geometry_object):
    """
    Generate the material color of the geometry object

    :param geometry_object: the geometry object
    :return:  the material of the geometry object
    """
    material = dict()
    material["color"] = get_color(geometry_object["type"],
                                  geometry_object["sub_type"] if "sub_type" in geometry_object else None)
    material["type"] = geometry_object["type"]
    material["sub_type"] = geometry_object["sub_type"] if "sub_type" in geometry_object else ""
    material_id = material["type"] + material["sub_type"]
    return material, material_id


def generate_height(building, scale):
    """
    Generate the height of the geometry object based on the scale

    for example the height of a building is based on the number of levels

    :param building: the geometry object
    :param scale: the scale used to generate the city
    :return: the height of a building
    """
    heigh = 0.0001
    if building["type"] == "building" and "level" in building:
        heigh = (building["level"] + scale / 20) / 20
    elif building["type"] == "water":
        heigh = 0.0001
    elif building["type"] == "highway":
        heigh = 0.0005
    return heigh


@app.route("/", methods=['GET', 'POST', 'DELETE'])
def retrieve_address():
    coord_statistics = CoordStatistics()

    address = request.form.get('address')
    print("address got: " + address)
    start = time.time()

    if not address: return json.dumps({"error": "error"}), 500
    try:
        point = ox.geocode(address)

        print(f"\tTime to retrieve point: {time.time() - start}")
        start = time.time()
        all_gemoetry_objects = dict()

        materials = dict()
        for footprint_type in ["building", "landuse", "water", "natural"]:
            generate_coordinates(point, all_gemoetry_objects, coord_statistics, distance=distance,
                                 footprint_type=footprint_type)
            print(f"\tTime to retrieve and generate {footprint_type}: {time.time() - start}")
            start = time.time()
        road_multigraph = ox.graph_from_point(point, distance)
        print(f"\tTime to retrieve roads: {time.time() - start}")
        start = time.time()

        generates_roads(road_multigraph, all_gemoetry_objects, coord_statistics)
        print(f"\tTime to generate roads: {time.time() - start}")

        start = time.time()
        for id, geometry_object in all_gemoetry_objects.items():
            # Scale geometry objects to make them fit in the scene
            height = generate_height(geometry_object, scale)
            geometry_object["level"] = height
            new_coords = []

            for x, y in geometry_object["coords"]:
                # Scale coords to fit in 3d space of the scene
                new_x = (x - coord_statistics.x_min) / (coord_statistics.x_max - coord_statistics.x_min) * scale - (
                        scale / 2)
                new_y = (y - coord_statistics.y_min) / (coord_statistics.y_max - coord_statistics.y_min) * scale - (
                        scale / 2)
                new_coords.append(new_x)
                new_coords.append(new_y)
            geometry_object["coords"] = new_coords
            material, material_id = generate_material(geometry_object)
            materials[material_id] = material
    except Exception as e:
        print(e)
        return json.dumps({"error": "error"}), 500
    print(f"\tTime to generate json: {time.time() - start}")
    return json.dumps({"buildings": all_gemoetry_objects, "materials": materials}, indent=4)


if __name__ == '__main__':
    app.run("0.0.0.0")
